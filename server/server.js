const path = require('path')
const http = require('http')
const express = require('express')
const socketIO = require('socket.io')

const {generateMessage, generateLocationMessage} = require('./utils/message')
const {isRealString} = require('./utils/validation')
const {Users} = require('./utils/users')

const publicPath = path.join(__dirname, '../public')
const port = process.env.PORT || 8080

// crearte web socket server
var app = express()
var server = http.createServer(app)
var io = socketIO(server)
var users = new Users()

// add public path to middleware
app.use(express.static(publicPath))


// register an event listener (az itt kiküldött eventek a betöltéskor mennek ki és jelennek meg ha arra a frontend is fel van készülve)
io.on('connection', (socket) => {
    console.log('new user connected')

    // //welcome new user - send message for one person
    // socket.emit('newMessage', generateMessage('Admin', 'Welcome to the chat app'))
    // // new user joined message for others, excluded the sender
    // socket.broadcast.emit('newMessage',  generateMessage('Admin', 'New user joined'))

    socket.on('join', (params, callack) => {
        if (!isRealString(params.name) || !isRealString(params.room)) {
            return callack('Name and room name are reqiured')
        }

        socket.join(params.room)
        users.removeUser(socket.id)
        users.addUser(socket.id, params.name, params.room)

        // azért az io.to.emit- mert mindenkinek kell a szobában 
        io.to(params.room).emit('updateUserList', users.getUserList(params.room))
        // csak egynek
        socket.emit('newMessage', generateMessage('Admin', 'Welcome to the chat app'))
        // mindenkinek kivéve a belépőt
        socket.broadcast.to(params.room).emit('newMessage',  generateMessage('Admin', `${params.name} has joined`))

        callack()
    })

    //handle custom event from frontend
    socket.on('createMessage', (message, callback) => {
        var user = users.getUser(socket.id)

        if (user && isRealString(message.text)) {
            //broadcast event for everyone - included the sender
            io.to(user.room).emit('newMessage', generateMessage(user.name, message.text))
        }

        
        callback()
    })

    //handle createLocationMessage event
    socket.on('createLocationMessage', (coords) => {
        var user = users.getUser(socket.id)
        io.to(user.room).emit('newLocationMessage', generateLocationMessage(user.name, coords.latitude, coords.longitude))
    })

    //event: disconnect
    socket.on('disconnect', () => {
        var user = users.removeUser(socket.id)
        if (user){
            io.to(user.room).emit('updateUserList', users.getUserList(user.room))
            io.to(user.room).emit('newMessage', generateMessage('Admin', `${user.name} has left the room`))
        }
        console.log('User was disconnected');
    })
})


server.listen(port, () => {
    console.log(`server is up on port ${port}`)
})