const expect = require('expect')
const {Users} = require('./users')

describe('Users', () => {
    var users

    beforeEach(() => {
        users = new Users()
        users.users = [{
            id: 1,
            name: 'Szil',
            room: 'Node'
        }, {
            id: 2,
            name: 'Jen',
            room: 'React'
        }, {
            id: 3,
            name: 'Julie',
            room: 'Node'
        }]
    })

    it ('should add new user', () => {
        var users = new Users()
        var user = {
            id: '234',
            name: 'Szil',
            room: 'got'
        }
        var resUser = users.addUser(user.id, user.name, user.room)
        expect(users.users).toEqual([user])
    })

    it ('should return names for react room', () => {
        var userList = users.getUserList('React')
        expect(userList).toEqual(['Jen'])
    })

    it ('should return names for node room', () => {
        var userList = users.getUserList('Node')
        expect(userList).toEqual(['Szil', 'Julie'])
    })

    it ('should remove a user', () => {
        var userList = users.removeUser(1)
        expect(userList).toEqual({id: 1, name: 'Szil', room: 'Node'})
    })

    it ('should not remove a user', () => {
        var userList = users.removeUser(4)
        expect(userList).toNotExist()
    })

    it ('should find user', () => {
        var userId = 2
        var user = users.getUser(userId)
        expect(user.id).toBe(userId)
    })

    it ('should not find user', () => {
        var userId = 4
        var user = users.getUser(userId)
        expect(user).toNotExist()
    })

    
})