[{
    id: 'dsadasdéasj',
    name: 'szil',
    room: 'room name'
}]

// addUser(id, name, room)
// removeUser(id)
// getUser(id)
// getUserList(room)

const _ = require('lodash')

class Users {
    constructor () {
        this.users = []
    }

    addUser (id, name, room) {
        var user = {
            id,
            name,
            room
        }
        this.users.push(user)
        return user
    }

    removeUser (id) {
        // my solution:
        // var removedUser = _.remove(this.users, (user) => user.id === id)
        // return removedUser[0]
        return _.remove(this.users, (user) => user.id === id)[0]

        // var user = this.getUser(id)
        // if (user) {
        //     this.users = this.users.filter((user) => user.id != id)
        // }
        // return user
    }

    getUser (id) {
        return this.users.filter((user) => user.id == id)[0]
    }

    getUserList (room) {
        var users = this.users.filter((user) => user.room == room)
        var namesArray = users.map((user) => user.name)

        return namesArray
    }

}

module.exports = {Users}


// ES6 Class syntax

// class Person {
//     constructor (name, age) {
//         this.name = name
//         this.age = age
//     }

//     getUserDescription () {
//         return `${this.name} is ${this.age} year(s) old `
//     }
// }

// var me = new Person('Szilárd', 29)
// console.log(me.getUserDescription());