var socket = io()

function scrollToBottom() {
    // selectors
    var messages = $('#messages')
    var newMessage = $('#messages').children('li:last-child')
    // heights
    var clientHeight = messages.prop('clientHeight')
    var scrollTop = messages.prop('scrollTop')
    var scrollHeight = messages.prop('scrollHeight')
    var newMessageHeight = newMessage.innerHeight() 
    var lastMessageHeight = newMessage.prev().innerHeight()

    if (clientHeight + scrollTop + newMessageHeight + lastMessageHeight >= scrollHeight){
        messages.scrollTop(scrollHeight)
    }else{
        //console.log(`${clientHeight + scrollTop + newMessageHeight + lastMessageHeight} < ${scrollHeight}`);
    }
}

// handle connection event in frontend /event listener/
socket.on('connect', function () {
    var params = $.deparam(window.location.search)
    socket.emit('join', params, function (err) {
        if (err) {
            alert(err)
            window.location.href = '/'
        } else {
            console.log('No error');
        }
    })
})

socket.on('disconnect', function () {
    console.log('Disconected form server');
})

socket.on('updateUserList', function (users) {
    var ol = $('<ol></ol>')
    users.forEach( function (user) {
        ol.append($('<li></li>').text(user))
    })
    $('#users').html(ol)
    console.log(users)
})

// handle incoming custom event
socket.on('newMessage', function (message) {
    var formatedTime = moment(message.createdAt).format('h:mm a')
    var template = $('#message-template').html()
    var html = Mustache.render(template, {
        text: message.text,
        from: message.from,
        createdAt: formatedTime,
    })

    $('#messages').append(html)
    scrollToBottom()

    // console.log('New message has arrived', message);
    // var li = $('<li></li>')
    // li.text(`${message.from} ${formatedTime}: ${message.text}`)

    // $('#messages').append(li)
})

socket.on('newLocationMessage', function (message) {
    var formatedTime = moment(message.createdAt).format('h:mm a')
    var template = $('#location-message-template').html()
    var html = Mustache.render(template, {
        url: message.url,
        from: message.from,
        createdAt: formatedTime,
    })

    $('#messages').append(html)
    scrollToBottom()

    // var li = $('<li></li>')
    // var a = $('<a target="_blank">My current location</a>')

    // li.text(`${message.from} ${formatedTime}: `)
    // a.attr('href', message.url)
    // li.append(a)

    // $('#messages').append(li)
})

// emit with callback() function: make sure the massage sent well or something was invalid,
// we have to call the callback function in serverside once that is processed and of course we have to add the callback parameter to the parameter list
/**
 * Handle click events
 */
$('#message-form').on('submit', function (event) {
    event.preventDefault()

    var messageTextbox = $('[name=message]')
    socket.emit('createMessage', {
        text: messageTextbox.val()
    }, function () {
        messageTextbox.val('')
    })
})

var locationBtn = $('#send-location')
locationBtn.on('click', function (event) {
    event.preventDefault()
    if (!navigator.geolocation){
        return alert('Use browser instead of potato')
    }
    locationBtn.attr('disabled', 'disabled').text('Sending location...')

    navigator.geolocation.getCurrentPosition(function (position) {
        locationBtn.removeAttr('disabled').text('Sending location')
        socket.emit('createLocationMessage', {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
        })
    }, function () {
        locationBtn.removeAttr('disabled').text('Sending location')
        alert('Unable to fetch location')
    })
})